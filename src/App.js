import './App.css';
import React, { useEffect, useState } from "react";
import axios from 'axios';
import { Helmet } from 'react-helmet';
import 'antd/dist/antd.css';
import { Layout } from 'antd';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import DeleteIcon from '@material-ui/icons/Delete'
// components
import Loading from './Loading';

// css  
import "./fontawsome.css";

const useStyles = makeStyles((theme) => ({
  root: {
          width: '100%',
          maxWidth: 400,
          backgroundColor: theme.palette.background.paper,
          flexGrow: 1,
          
  },
  demo: {
          backgroundColor: theme.palette.background.paper,
  },
  title: {
          margin: theme.spacing(4, 0, 2),
  },
  font:{
        fontSize:'3em'
  }
}));


// ********* WaybillGrid component **************
const { Content } = Layout;
  
function App() {
  const [products, setProducts] = useState([]);
  const [shoppingBasket, setShoppingBasket] = useState([]);
  const classes = useStyles();
  const [dense, setDense] = React.useState(false);
  const [totalPrice, setTotalPrice] = React.useState(0);

  useEffect(() => {
          axios.get(`https://fakestoreapi.com/products`)
                  .then(res => {
                          const products = res.data;
                          setProducts(products.slice(0, 5))
                  })
  }, []); // useEffect

  const handleAddToCard = (e, value) => {
          let id
          shoppingBasket.map((item, i) => {
                  if (item.id === value.id) {
                          id = true
                          shoppingBasket[i].count++
                          setShoppingBasket([...shoppingBasket])
                          setTotalPrice(Number(((totalPrice + item.price).toFixed(2))))
                  }

          })// map for checking if this item already  exist in shopping cart
          if (id) return null
          shoppingBasket.push({ ...value, count: 1 })
          setTotalPrice(Number(((totalPrice + value.price).toFixed(2))))
          setShoppingBasket([...shoppingBasket])
  }// add items to cart in product list

  const subtractOrder = (id) => {
          shoppingBasket.map((item, index) => {
                  if (item.id === id && shoppingBasket[index].count !==0) {
                          shoppingBasket[index].count--
                          setShoppingBasket([...shoppingBasket])
                          console.log(shoppingBasket[index].count);
                          setTotalPrice(Number(((totalPrice - item.price).toFixed(2))))
                  }
          })
  }// subtract number of ordered item

  const addOrder = (id) => {
          shoppingBasket.map((item, index) => {
                  if (item.id === id) {
                          shoppingBasket[index].count++
                          setShoppingBasket([...shoppingBasket])
                          setTotalPrice(Number(((totalPrice + item.price).toFixed(2))))
                  }
          })
  }// plus number of ordered item

  const handleDeleteItem = (item) => {
          shoppingBasket.map((basket, index) => {
                  if (basket.id === item.id) {
                          shoppingBasket.splice(index, 1)
                          setTotalPrice(Number(((totalPrice - (item.price * item.count)).toFixed(2))))
                          setShoppingBasket([...shoppingBasket])
                          return null
                  }
          })
  }// delete item from basket(this wasnt part of Project)

  const handleResetBasket = () => {
          shoppingBasket.map((basket, index) => {
                  shoppingBasket[index].count = 0
          })
          setTotalPrice(0)
          setShoppingBasket([...shoppingBasket])
  }// reset number and price of item in basket
  if (!products.length) return <Loading />
  return (
    <div className="App">
       <React.Fragment>
                        <Helmet>
                                <title>Product List and basket</title>
                        </Helmet>
                        <Layout>
                                <Layout>
                                        <Content>
                                                <div className="productTitle col-lg-12 col-md-12 col-sm-12 col-xs-12">Product List</div>
                                                <List dense className={`${classes.root} mainContent col-lg-6 col-md-8 col-sm-8 col-xs-8`}>
                                                        {/* product List  section*/}
                                                        {products.map((value) => {
                                                                const labelId = `checkbox-list-secondary-label-${value.id}`;
                                                                const title = value.title.length > 12 ? value.title.substring(0, 12) + '...' : value.title
                                                                return (
                                                                        <ListItem key={value.id} button>
                                                                                <ListItemAvatar>
                                                                                        <Avatar
                                                                                                alt={`Avatar n°${value + 1}`}
                                                                                                src={`/static/images/avatar/${value + 1}.jpg`}
                                                                                        />
                                                                                </ListItemAvatar>
                                                                                <ListItemText className={classes.font} id={labelId} primary={`${value.id}  ${title}  `} />
                                                                                <ListItemSecondaryAction>
                                                                                        <IconButton color="primary" aria-label="add to shopping cart" onClick={(e) => handleAddToCard(e, value)}>
                                                                                                <AddShoppingCartIcon />
                                                                                        </IconButton>
                                                                                </ListItemSecondaryAction>
                                                                        </ListItem>
                                                                );
                                                        })}
                                                        {/* End of product List  section*/}
                                                </List>
                                                <div className={`shoppingBasket col-lg-4 col-md-4 col-sm-12 col-xs-12`}>
                                                        <ul className="shoppingBasket_ul col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                {shoppingBasket.map((item) => {
                                                                        return <li className="shoppingBasket_li col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                                                                                <span className="col-lg-3 col-md-4 col-sm-12 col-xs-12" >{item.title.substring(0, 12)}</span>
                                                                                <button className="col-lg-1 col-md-1 col-sm-1 col-xs-1" onClick={() => subtractOrder(item.id)}>-</button>
                                                                                <input className="col-lg-3 col-md-3 col-sm-8 col-xs-7" type="text" name={`input${item.id}`} value={item.count} />
                                                                                <button className="col-lg-1 col-md-1 col-sm-1 col-xs-1" onClick={() => addOrder(item.id)}>+</button>
                                                                                <span className="col-lg-3 col-md-8 col-sm-9 col-xs-9">{Number(((item.price * item.count).toFixed(2)))} $</span>
                                                                                <button className="col-lg-1 col-md-1 col-sm-1 col-xs-1" onClick={() => handleDeleteItem(item)}>X</button>

                                                                                {/* <IconButton edge="end" aria-label="delete" onClick={() => handleDeleteItem(item)}>
                                                                                        <DeleteIcon />
                                                                                </IconButton> */}
                                                                        </li>
                                                                })
                                                                }
                                                                {/* End Of shoppingBaskett  section*/}
                                                        </ul>
                                                        {/* <br/> */}
                                                        {/* <hr /> */}
                                                        <span> TotalPrice </span> {totalPrice} <span> $ </span>
                                                        <button onClick={() => { handleResetBasket() }}>Clear</button>
                                                        {/* </div> */}
                                                </div>
                                        </Content>
                                </Layout>
                        </Layout>
                </React.Fragment>
    </div>
  );
}

export default App;

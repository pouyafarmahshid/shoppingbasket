import React from 'react'


const loading= (props)=>{
    
    return(
        <div role="dialog" className="loadingParent col-lg-12 col-md-12 col-sm-12 col-xs-12" >
            <div className="loadingBack col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className=" col-lg-12 col-md-12 col-sm-12 col-xs-12 loadingText"><label>Loading ...</label></div>
            </div>{/* back loading */}
            
        </div>// loadingBack
    )//return
}//loading

export default loading;